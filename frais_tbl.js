$(document).ready(function(){
    function SupprimerLigne(evt)
    {
        var $btnSupprimer = $(evt.target);
        var $lig = $btnSupprimer.parents("tr");
        $lig.remove();
    }


    var $idxLigne = 1000001;
    $(".btn-ajouter").click(function(evt){
        var $tplLigne = $("#fraisreelsnewline");
        var $btnAjouter = $(evt.target);
        var act = $btnAjouter.data('act');
        var $ligne = $($tplLigne.html());    // clone les éléments du template

        $ligne.find(".js-aaid").val($btnAjouter.data('act'));
        $ligne.find(".js-aaid").attr("name", "aaid[]");
        $ligne.find(".js-aaid").attr("value", act);
        // console.log('hello');
        var $tableauSrc = $("#frais-"+act+" tbody"); // le tableau des sources

        // ... autres initialisations
        // $ligne.find(".text-src-desc").attr("name", "descsource[]");
        // $ligne.find(".text-src-lieu").attr("name", "lieu[]");
        // $ligne.find(".text-src-url").attr("name", "url[]");
        // $ligne.find(".text-src-aacquerir").attr("name", "aacquerir[]");
        // $ligne.find(".text-src-aacquerir-value").attr("name", "aacquerirvalue[]");


        $ligne.find(".btn-supprimer").click(SupprimerLigne);

        $idxLigne++;

        $tableauSrc.append($ligne);
    });


    $(".btn-supprimer").click(SupprimerLigne);

});