<form action="{jurl 'fraisreels~default:test'}" method="post">
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                {foreach $attribs as $a}
                    <table class="table table-sm table-bordered border-dark" id="">
                        <thead>
                        <tr>
                            <th style="width:5%;" class="text-center align-middle"></th>
                            <th style="width:20%;" class="text-center align-middle">Type de dépense</th>
                            <th style="width:50%;" class="text-center align-middle">Libellé</th>
                            <th style="width:10%;" class="text-center align-middle">€</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="2"
                                class="bg-primary bg-gradient fw-bold align-middle text-white">{$a[0]->ue_bloc} {$a[0]->ori_lib_court}</td>
                            <td class="bg-primary bg-gradient fw-bold text-center align-middle" colspan="2"><a href=""
                                                                                                               class="btn btn-warning btn-block"><i
                                            class="fa-solid fa-magnifying-glass"></i> Voir l'ensemble des frais réels
                                    introduits par rapport à cette formation</a></td>
                        </tr>
                        {assign $pp = 90000}
                        {foreach $a as $aa}
                            <tr>
                                <td class="text-center align-middle">
                                    <button type="button" class="btn btn-success btn-ajouter" data-act=""><i
                                                class="fa-solid fa-plus"></i></button>
                                </td>
                                <td colspan="3" class="align-middle">{$aa->aa_lib}</td>
                            </tr>
                            <tr>
                                <td class="text-center align-middle">
                                    <button type="button" class="btn btn-danger btn-supprimer"><i
                                                class="fa-solid fa-trash-can"></i></button>
                                    <input type="hidden" class="activite js-aaid" name="aaid[{$lids}]" id="aaid[]"
                                           value="{$aa->aa_id}"/>
                                </td>
                                <td><select name="typefrais[{$lids}]" id="typefrais[]" class="form-select js-typefrais">
                                        <option value="N/C"></option>
                                        <option value="M">Matériel didactique</option>
                                        <option value="O">Ouvrages de référence</option>
                                        <option value="S">Séminaires, visites, voyages</option>
                                    </select></td>
                                <td><input type="text" class="form-control js-libelle"
                                           placeholder="exemple: Livre ... (avec références) - voyage - blouse de labo"
                                           id="libelle[]" name="libelle[{$lids}]">
                                </td>
                                <td><input type="text" class="form-control js-prix" placeholder="Prix approximatif"
                                           id="prix[]" name="prix[{$lids}]"></td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                {/foreach}
            </div>
        </div>
    </div>
    <button type="submit">test</button>
</form>
