<?php
/**
* @package   hetools
* @subpackage fraisreels
* @author    your name
* @copyright 2011 your name
* @link      http://www.yourwebsite.undefined
* @license    All rights reserved
*/

//jClasses::inc('fraisreels~archi');
jClasses::inc('fraisreels~attributions');
class defaultCtrl extends jController {
    public $pluginParams = array(
        '*' => array('auth.required' => true,
            'jacl2.right'=>'50')
    );
    /**
    *
    */
    private function setUsrid()
    {
        $svuser = jAuth::getUserSession();
        return $svuser->ctcsid();
    }

    function index() {
        //        $n = new archi();
        //        $n->architectures();

        $usr = $this->setUsrid();

        $attrib = new attributions();
        $attributions = $attrib->getUsrAttributions($usr);

        $rep = $this->getResponse('heldb');
        $a = jApp::urlBasePath();
        $rep->addJSLink($a.'js/frais_tbl.js');
        $rep->title = 'Gestion des Frais réels';
        $tpl = new jTpl();
        $tpl->assign('attribs', $attributions);
        $rep->body->assign('content', $tpl->fetch('fraisreels~attribsvsfrais'));

        return $rep;
    }

    function test(){
        $p = $this->params();
        var_dump($p);
        die();
    }
}

