<?php
ini_set('xdebug.var_display_max_depth', -1);
ini_set('xdebug.var_display_max_children', -1);
ini_set('xdebug.var_display_max_data', -1);
class attributions {
    public $usr = null;

    public function getUsrAttributions($usr){
        $usr = 207;
        $this->usr = $usr;

        $dao = jDao::get("fraisreels~attributionsvscoursvsens","hetools");
        $conditions = jDao::createConditions();
        $conditions->addCondition('mdp_id','=',$usr);
        $conditions->addCondition('anac_id','=',8);

        $liste = $dao->findBy($conditions);

        $listerec = array();
        foreach($liste as $v){
            $listerec[$v->aa_id] = $v->aa_id;
        }

        $architecture = jDao::get("fraisreels~varchitecture","hetools");
        $conditions2 = jDao::createConditions();
        $conditions2->addCondition('aa_id','IN',$listerec);
        $conditions2->addCondition('anac_id','=',8);
        $conditions->addItemOrder('ue_bloc','asc');
        $conditions->addItemOrder('ue_numue','asc');
        $conditions->addItemOrder('aa_lib','asc');

        $liste2 = $architecture->findBy($conditions2);

        $liste2rec = array();
        foreach($liste2 as $v2){
            $liste2rec[$v2->sod_id][] = $v2;
        }

        return $liste2rec;

    }
}